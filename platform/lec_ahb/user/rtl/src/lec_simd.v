// ****************************************************************************
// ****************************************************************************
// Copyright SoC Design Research Group, All rights reserved.    
// Electronics and Telecommunications Research Institute (ETRI)
//
// THESE DOCUMENTS CONTAIN CONFIDENTIAL INFORMATION AND KNOWLEDGE 
// WHICH IS THE PROPERTY OF ETRI. NO PART OF THIS PUBLICATION IS 
// TO BE USED FOR ANY OTHER PURPOSE, AND THESE ARE NOT TO BE 
// REPRODUCED, COPIED, DISCLOSED, TRANSMITTED, STORED IN A RETRIEVAL 
// SYSTEM OR TRANSLATED INTO ANY OTHER HUMAN OR COMPUTER LANGUAGE, 
// IN ANY FORM, BY ANY MEANS, IN WHOLE OR IN PART, WITHOUT THE 
// COMPLETE PRIOR WRITTEN PERMISSION OF ETRI.
// ****************************************************************************
// Kyuseung Han (han@etri.re.kr)
// ****************************************************************************
// ****************************************************************************

`include "ervp_global.vh"
`include "ervp_endian.vh"
`include "ervp_ahb_define.vh"
`include "lec_simd_memorymap_offset.vh"

module LEC_SIMD
(
	clk,
	rstnn,

	shaddr,
	shburst,
	shmasterlock,
	shprot,
	shsize,
	shtrans,
	shwrite,
	shwdata,
	shready,
	shrdata,
	shresp,

	rpsel,
	rpenable,
	rpaddr,
	rpwrite,
	rpwdata,
	rprdata,
	rpready,
	rpslverr
);

////////////////////////////
/* parameter input output */
////////////////////////////

localparam BW_ADDR = 32;
localparam BW_DATA = 32;
localparam BW_BYTE = 8;
localparam NUM_BYTE_IN_DATA = BW_DATA/BW_BYTE;

parameter ENDIAN_TYPE = `LITTLE_ENDIAN;

localparam BURST_LENGTH = 16;
localparam BW_BURST_LENGTH_COUNTER = 4;

input wire clk;
input wire rstnn;

output reg [BW_ADDR-1:0] shaddr;
output wire [`BW_AHB_BURST-1:0] shburst;
output wire shmasterlock;
output wire [`BW_AHB_PROT-1:0] shprot;
output wire [`BW_AHB_SIZE-1:0] shsize;
output reg [`BW_AHB_TRANS-1:0] shtrans;
output wire shwrite;
output wire [BW_DATA-1:0] shwdata;
input wire shready;
input wire [BW_DATA-1:0] shrdata;
input wire shresp;

input wire rpsel;
input wire rpenable;
input wire [BW_ADDR-1:0] rpaddr;
input wire rpwrite;
input wire [BW_DATA-1:0] rpwdata;
output wire [BW_DATA-1:0] rprdata;
output wire rpready;
output wire rpslverr;

/////////////
/* signals */
/////////////

genvar i;

wire [32-1:0] lec_simd_src0_addr;
wire [32-1:0] lec_simd_src1_addr;
wire [32-1:0] lec_simd_dst_addr;
wire [32-1:0] lec_simd_num_data;
wire lec_simd_start_write_cmd;
wire lec_simd_state_re;
wire [2-1:0] lec_simd_state_input;

wire [2-1:0] src0_wready;
wire src0_wrequest;
wire [BW_DATA-1:0] src0_wdata;
wire src0_rready;
wire src0_rrequest;
wire [BW_DATA-1:0] src0_rdata;

wire [2-1:0] src1_wready;
wire src1_wrequest;
wire [BW_DATA-1:0] src1_wdata;
wire src1_rready;
wire src1_rrequest;
wire [BW_DATA-1:0] src1_rdata;

wire dst_wready;
wire dst_wrequest;
wire [BW_DATA-1:0] dst_wdata;
wire [2-1:0] dst_rready;
wire dst_rrequest;
wire [BW_DATA-1:0] dst_rdata;

localparam STATE_IDLE = 0;
localparam STATE_READ0 = 1;
localparam STATE_READ1 = 2;
localparam STATE_WRITE = 3;
localparam BW_STATE = 2;

reg [BW_STATE-1:0] addr_phase_state, data_phase_state;
wire addr_phase_state_is_valid;
wire data_phase_state_is_valid;

wire is_last_iteration;
wire is_last_addr_transfer;

wire master_is_busy;
reg will_master_be_busy;

reg [32-1:0] num_remaining_data;

wire counter_init;
wire counter_count;
wire counter_is_first_count;
wire counter_is_last_count;

reg is_addr_tranfer_requested;
wire is_addr_tranfer_complete;
wire is_last_addr_tranfer_complete;

wire is_data_tranfer_complete;

wire is_computation_performed;

reg [BW_ADDR-1:0] src0_addr;
reg [BW_ADDR-1:0] src1_addr;
reg [BW_ADDR-1:0] dst_addr;
wire [BW_ADDR-1:0] shaddr_next;

////////////
/* logics */
////////////

LEC_SIMD_MMIO
#(
	.BW_ADDR(BW_ADDR),
	.BW_DATA(BW_DATA),
	.ENDIAN_TYPE(ENDIAN_TYPE)
)
i_mmio
(
	.clk(clk),
	.rstnn(rstnn),

	.rpsel(rpsel),
	.rpenable(rpenable),
	.rpaddr(rpaddr),
	.rpwrite(rpwrite),
	.rpwdata(rpwdata),
	.rprdata(rprdata),
	.rpready(rpready),
	.rpslverr(rpslverr),

	.lec_simd_src0_addr(lec_simd_src0_addr),
	.lec_simd_src1_addr(lec_simd_src1_addr),
	.lec_simd_dst_addr(lec_simd_dst_addr),
	.lec_simd_num_data(lec_simd_num_data),
	.lec_simd_start_write_cmd(lec_simd_start_write_cmd),
	.lec_simd_state_re(lec_simd_state_re),
	.lec_simd_state_input(lec_simd_state_input)
);

assign lec_simd_state_input = addr_phase_state;

ERVP_FIFO
#(
	.BW_DATA(BW_DATA),
	.DEPTH(BURST_LENGTH),
	.WRITE_READY_SIZE(2)
)
i_src0_fifo
(
	.clk(clk),
	.rstnn(rstnn),
	.enable(1'b 1),
  .clear(1'b 0),
	.wready(src0_wready),
	.wfull(),
	.wrequest(src0_wrequest),
	.wdata(src0_wdata),
	.wnum(),
	.rready(src0_rready),
	.rempty(),
	.rrequest(src0_rrequest),
	.rdata(src0_rdata),
	.rnum()
);

assign src0_wrequest = (data_phase_state==STATE_READ0) & is_data_tranfer_complete;
assign src0_wdata = shrdata;

ERVP_FIFO
#(
	.BW_DATA(BW_DATA),
	.DEPTH(BURST_LENGTH),
	.WRITE_READY_SIZE(2)
)
i_src1_fifo
(
	.clk(clk),
	.rstnn(rstnn),
	.enable(1'b 1),
  .clear(1'b 0),
	.wready(src1_wready),
	.wfull(),
	.wrequest(src1_wrequest),
	.wdata(src1_wdata),
	.wnum(),
	.rready(src1_rready),
	.rempty(),
	.rrequest(src1_rrequest),
	.rdata(src1_rdata),
	.rnum()
);

assign src1_wrequest = (data_phase_state==STATE_READ1) & is_data_tranfer_complete;
assign src1_wdata = shrdata;

ERVP_FIFO
#(
	.BW_DATA(BW_DATA),
	.DEPTH(BURST_LENGTH),
	.READ_READY_SIZE(2)
)
i_dst_fifo
(
	.clk(clk),
	.rstnn(rstnn),
	.enable(1'b 1),
  .clear(1'b 0),
	.wready(dst_wready),
	.wfull(),
	.wrequest(dst_wrequest),
	.wdata(dst_wdata),
	.wnum(),
	.rready(dst_rready),
	.rempty(),
	.rrequest(dst_rrequest),
	.rdata(dst_rdata),
	.rnum()
);

assign dst_rrequest = (data_phase_state==STATE_WRITE) & is_data_tranfer_complete;
assign shwdata = dst_rdata;

// phase_state //

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
	begin
		addr_phase_state <= STATE_IDLE;
		shtrans <= `AHB_TRANS_IDLE;
	end
	else
		case(addr_phase_state)
			STATE_IDLE:
			begin
				if(lec_simd_start_write_cmd)
				begin
					addr_phase_state <= STATE_READ0;
					shtrans <= `AHB_TRANS_NONSEQ;
				end
			end
			STATE_READ0:
			begin
				if(is_last_addr_tranfer_complete)
				begin
					addr_phase_state <= STATE_READ1;
					shtrans <= `AHB_TRANS_NONSEQ;
				end
				else if(will_master_be_busy)
					shtrans <= `AHB_TRANS_BUSY;
				else
					shtrans <= `AHB_TRANS_SEQ;
			end
			STATE_READ1:
			begin
				if(is_last_addr_tranfer_complete)
				begin
					addr_phase_state <= STATE_WRITE;
					shtrans <= `AHB_TRANS_NONSEQ;
				end
				else if(will_master_be_busy)
					shtrans <= `AHB_TRANS_BUSY;
				else
					shtrans <= `AHB_TRANS_SEQ;
			end
			STATE_WRITE:
			begin
				if(is_last_addr_tranfer_complete)
				begin
					if(is_last_iteration)
					begin
						addr_phase_state <= STATE_IDLE;
						shtrans <= `AHB_TRANS_IDLE;
					end
					else
					begin
						addr_phase_state <= STATE_READ0;
						shtrans <= `AHB_TRANS_NONSEQ;
					end
				end
				else if(will_master_be_busy)
					shtrans <= `AHB_TRANS_BUSY;
				else
					shtrans <= `AHB_TRANS_SEQ;
			end
		endcase
end

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
		data_phase_state <= STATE_IDLE;
	else if(~master_is_busy)
		data_phase_state <= addr_phase_state;
end

assign addr_phase_state_is_valid = (addr_phase_state!=STATE_IDLE) && (~master_is_busy);
assign data_phase_state_is_valid = (data_phase_state!=STATE_IDLE);

// is_last_iteration //

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
		num_remaining_data <= 0;
	else if(lec_simd_start_write_cmd)
		num_remaining_data <= lec_simd_num_data;
	else if((addr_phase_state==STATE_WRITE)&& is_last_addr_tranfer_complete)
	begin
		if(is_last_iteration)
			num_remaining_data <= 0;
		else
			num_remaining_data <= num_remaining_data-BURST_LENGTH;
	end
end

assign is_last_iteration = (num_remaining_data<=BURST_LENGTH);

// is_last_addr_transfer //

ERVP_COUNTER
#(
	.BW_COUNTER(BW_BURST_LENGTH_COUNTER),
	.LAST_NUMBER(BURST_LENGTH-1)
)
i_counter
(
	.clk(clk),
	.rstnn(rstnn),
	.enable(1'b 1),
	.init(counter_init),
	.count(counter_count),
	.value(),
	.is_first_count(counter_is_first_count),
	.is_last_count(counter_is_last_count)
);

assign counter_init = is_last_addr_tranfer_complete;
assign counter_count = is_addr_tranfer_complete;
assign is_last_addr_transfer = counter_is_last_count;

// master_is_busy //

assign master_is_busy = (shtrans==`AHB_TRANS_BUSY);

always@(*)
begin
	will_master_be_busy = 0;
	if(~is_last_addr_transfer)
		case(addr_phase_state)
			STATE_READ0:
			begin
				if(addr_phase_state_is_valid && data_phase_state_is_valid)
					will_master_be_busy = ~src0_wready[1];
				else
					will_master_be_busy = ~src0_wready[0];
			end
			STATE_READ1:
			begin
				if(addr_phase_state_is_valid && data_phase_state_is_valid)
					will_master_be_busy = ~src1_wready[1];
				else
					will_master_be_busy = ~src1_wready[0];
			end
			STATE_WRITE:
			begin
				if(addr_phase_state_is_valid && data_phase_state_is_valid)
					will_master_be_busy = ~dst_rready[1];
				else
					will_master_be_busy = ~dst_rready[0];
			end
		endcase
end

// transfer //

always@(*)
begin
	is_addr_tranfer_requested = 0;
	case(shtrans)
		`AHB_TRANS_NONSEQ,
		`AHB_TRANS_SEQ:
			is_addr_tranfer_requested = 1;
	endcase
end

assign is_addr_tranfer_complete = is_addr_tranfer_requested & shready;
assign is_last_addr_tranfer_complete = is_last_addr_transfer & is_addr_tranfer_complete;
assign is_data_tranfer_complete = data_phase_state_is_valid & (~master_is_busy) & shready;

// AHB //

assign shaddr_next = shaddr + 4;

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
	begin
		shaddr <= 0;
		src0_addr <= 0;
		src1_addr <= 0;
		dst_addr <= 0;
	end
	else
		case(addr_phase_state)
			STATE_IDLE:
			begin
				if(lec_simd_start_write_cmd)
				begin
					shaddr <= lec_simd_src0_addr;
					src0_addr <= lec_simd_src0_addr;
					src1_addr <= lec_simd_src1_addr;
					dst_addr <= lec_simd_dst_addr;
				end
			end
			STATE_READ0:
			begin
				if(is_last_addr_tranfer_complete)
				begin
					shaddr <= src1_addr;
					src0_addr <= shaddr_next;
				end
				else if(is_addr_tranfer_complete)
					shaddr <= shaddr_next;
			end
			STATE_READ1:
			begin
				if(is_last_addr_tranfer_complete)
				begin
					shaddr <= dst_addr;
					src1_addr <= shaddr_next;
				end
				else if(is_addr_tranfer_complete)
					shaddr <= shaddr_next;
			end
			STATE_WRITE:
			begin
				if(is_last_addr_tranfer_complete)
				begin
					shaddr <= src0_addr;
					dst_addr <= shaddr_next;
				end
				else if(is_addr_tranfer_complete)
					shaddr <= shaddr_next;
			end
		endcase
end

assign shprot = 0;
assign shmasterlock = 0;
assign shburst = `AHB_BURST_INCR16;
assign shsize= `AHB_SIZE_004BYTE;
assign shwrite = (addr_phase_state==STATE_WRITE);


// computation
generate
	for(i=0; i<NUM_BYTE_IN_DATA; i=i+1)
	begin  : i_add_byte
		assign dst_wdata[(i+1)*BW_BYTE-1-:BW_BYTE] = src0_rdata[(i+1)*BW_BYTE-1-:BW_BYTE] + src1_rdata[(i+1)*BW_BYTE-1-:BW_BYTE];
	end
endgenerate

assign is_computation_performed = src0_rready & src1_rready & dst_wready;
assign src0_rrequest = is_computation_performed;
assign src1_rrequest = is_computation_performed;
assign dst_wrequest = is_computation_performed;

endmodule

// ****************************************************************************
// ****************************************************************************
// Copyright SoC Design Research Group, All rights reserved.    
// Electronics and Telecommunications Research Institute (ETRI)
//
// THESE DOCUMENTS CONTAIN CONFIDENTIAL INFORMATION AND KNOWLEDGE 
// WHICH IS THE PROPERTY OF ETRI. NO PART OF THIS PUBLICATION IS 
// TO BE USED FOR ANY OTHER PURPOSE, AND THESE ARE NOT TO BE 
// REPRODUCED, COPIED, DISCLOSED, TRANSMITTED, STORED IN A RETRIEVAL 
// SYSTEM OR TRANSLATED INTO ANY OTHER HUMAN OR COMPUTER LANGUAGE, 
// IN ANY FORM, BY ANY MEANS, IN WHOLE OR IN PART, WITHOUT THE 
// COMPLETE PRIOR WRITTEN PERMISSION OF ETRI.
// ****************************************************************************
// Kyuseung Han (han@etri.re.kr)
// ****************************************************************************
// ****************************************************************************

`include "ervp_global.vh"
`include "ervp_endian.vh"
`include "lec_simd_memorymap_offset.vh"

module LEC_SIMD_MMIO
(
	clk,
	rstnn,

	rpsel,
	rpenable,
	rpaddr,
	rpwrite,
	rpwdata,
	rprdata,
	rpready,
	rpslverr,

	lec_simd_src0_addr,
	lec_simd_src1_addr,
	lec_simd_dst_addr,
	lec_simd_num_data,
	lec_simd_start_write_cmd,
	lec_simd_state_re,
	lec_simd_state_input
);

////////////////////////////
/* parameter input output */
////////////////////////////

parameter BW_ADDR = 1;
parameter BW_DATA = 1;
parameter ENDIAN_TYPE = `LITTLE_ENDIAN;

`include "ervp_endian.vf"
`include "ervp_log_util.vf"

input wire clk, rstnn;
input wire rpsel;
input wire rpenable;
input wire [BW_ADDR-1:0] rpaddr;
input wire rpwrite;
input wire [BW_DATA-1:0] rpwdata;
output wire [BW_DATA-1:0] rprdata;
output wire rpready;
output reg rpslverr;

output wire [32-1:0] lec_simd_src0_addr;

output wire [32-1:0] lec_simd_src1_addr;

output wire [32-1:0] lec_simd_dst_addr;

output wire [32-1:0] lec_simd_num_data;

output wire lec_simd_start_write_cmd;

output wire lec_simd_state_re;
input wire [2-1:0] lec_simd_state_input;

/////////////
/* signals */
/////////////

genvar i;

wire [BW_DATA-1:0] man_rpwdata;
reg [BW_DATA-1:0] man_rprdata;
wire read_request_from_bus;
wire write_request_from_bus;
wire is_aligned_access;

wire [`BW_MMAP_OFFSET_LEC_SIMD-1:0] paddr_offset = rpaddr;
wire [`BW_MMAP_OFFSET_LEC_SIMD-1:0] addr_offset;
wire [(`BW_MMAP_OFFSET_LEC_SIMD-`BW_UNUSED_LEC_SIMD)-1:0] addr_aligned;
wire [`BW_UNUSED_LEC_SIMD-1:0] addr_unaligned;
wire [`BW_UNUSED_LEC_SIMD-1:0] addr_unused = 0;
reg signal_lec_simd_src0_addr_re;
wire [32-1:0] signal_lec_simd_src0_addr_input;
reg signal_lec_simd_src0_addr_we;
wire [32-1:0] signal_lec_simd_src0_addr_output;
reg [32-1:0] reg_lec_simd_src0_addr;
reg signal_lec_simd_src1_addr_re;
wire [32-1:0] signal_lec_simd_src1_addr_input;
reg signal_lec_simd_src1_addr_we;
wire [32-1:0] signal_lec_simd_src1_addr_output;
reg [32-1:0] reg_lec_simd_src1_addr;
reg signal_lec_simd_dst_addr_re;
wire [32-1:0] signal_lec_simd_dst_addr_input;
reg signal_lec_simd_dst_addr_we;
wire [32-1:0] signal_lec_simd_dst_addr_output;
reg [32-1:0] reg_lec_simd_dst_addr;
reg signal_lec_simd_num_data_re;
wire [32-1:0] signal_lec_simd_num_data_input;
reg signal_lec_simd_num_data_we;
wire [32-1:0] signal_lec_simd_num_data_output;
reg [32-1:0] reg_lec_simd_num_data;
reg signal_lec_simd_start_re;
wire [32-1:0] signal_lec_simd_start_input;
reg signal_lec_simd_start_we;
wire [32-1:0] signal_lec_simd_start_output;
reg signal_lec_simd_state_re;
wire [2-1:0] signal_lec_simd_state_input;
reg signal_lec_simd_state_we;
wire [2-1:0] signal_lec_simd_state_output;

////////////
/* logics */
////////////

assign man_rpwdata = CHANGE_ENDIAN_BUS2MAN(BW_DATA,ENDIAN_TYPE,rpwdata);
assign rprdata = CHANGE_ENDIAN_MAN2BUS(BW_DATA,ENDIAN_TYPE,man_rprdata);
assign {addr_aligned,addr_unaligned} = paddr_offset;
assign addr_offset = {addr_aligned,addr_unused};
assign is_aligned_access = (addr_unaligned==0);
assign read_request_from_bus = rpsel & rpenable & is_aligned_access & (~rpwrite);
assign write_request_from_bus = rpsel & rpenable & is_aligned_access & rpwrite;
assign rpready = 1'b 1;

assign signal_lec_simd_src0_addr_output = $unsigned(rpwdata);
assign signal_lec_simd_src1_addr_output = $unsigned(rpwdata);
assign signal_lec_simd_dst_addr_output = $unsigned(rpwdata);
assign signal_lec_simd_num_data_output = $unsigned(rpwdata);
assign signal_lec_simd_start_output = $unsigned(rpwdata);
assign signal_lec_simd_state_output = $unsigned(rpwdata);

always@(*)
begin
	rpslverr = 0;
	man_rprdata = 0;
	signal_lec_simd_src0_addr_re = 0;
	signal_lec_simd_src0_addr_we = 0;

	signal_lec_simd_src1_addr_re = 0;
	signal_lec_simd_src1_addr_we = 0;

	signal_lec_simd_dst_addr_re = 0;
	signal_lec_simd_dst_addr_we = 0;

	signal_lec_simd_num_data_re = 0;
	signal_lec_simd_num_data_we = 0;

	signal_lec_simd_start_re = 0;
	signal_lec_simd_start_we = 0;

	signal_lec_simd_state_re = 0;
	signal_lec_simd_state_we = 0;

	if(rpsel==1'b 1)
	begin
		case(addr_offset)
			`MMAP_OFFSET_LEC_SIMD_SRC0_ADDR:
			begin
				signal_lec_simd_src0_addr_re = read_request_from_bus;
				signal_lec_simd_src0_addr_we = write_request_from_bus;
				man_rprdata = $unsigned(signal_lec_simd_src0_addr_input);
			end
			`MMAP_OFFSET_LEC_SIMD_SRC1_ADDR:
			begin
				signal_lec_simd_src1_addr_re = read_request_from_bus;
				signal_lec_simd_src1_addr_we = write_request_from_bus;
				man_rprdata = $unsigned(signal_lec_simd_src1_addr_input);
			end
			`MMAP_OFFSET_LEC_SIMD_DST_ADDR:
			begin
				signal_lec_simd_dst_addr_re = read_request_from_bus;
				signal_lec_simd_dst_addr_we = write_request_from_bus;
				man_rprdata = $unsigned(signal_lec_simd_dst_addr_input);
			end
			`MMAP_OFFSET_LEC_SIMD_NUM_DATA:
			begin
				signal_lec_simd_num_data_re = read_request_from_bus;
				signal_lec_simd_num_data_we = write_request_from_bus;
				man_rprdata = $unsigned(signal_lec_simd_num_data_input);
			end
			`MMAP_OFFSET_LEC_SIMD_START:
			begin
				signal_lec_simd_start_re = read_request_from_bus;
				signal_lec_simd_start_we = write_request_from_bus;
				man_rprdata = $unsigned(signal_lec_simd_start_input);
			end
			`MMAP_OFFSET_LEC_SIMD_STATE:
			begin
				signal_lec_simd_state_re = read_request_from_bus;
				signal_lec_simd_state_we = write_request_from_bus;
				man_rprdata = $unsigned(signal_lec_simd_state_input);
			end
			default:
				rpslverr = 1;
		endcase
	end
end

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
		reg_lec_simd_src0_addr <= `LEC_SIMD_SRC0_ADDR_DEFAULT_VALUE;
	else if (signal_lec_simd_src0_addr_we==1'b 1)
		reg_lec_simd_src0_addr <= signal_lec_simd_src0_addr_output;
end
assign signal_lec_simd_src0_addr_input = reg_lec_simd_src0_addr;

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
		reg_lec_simd_src1_addr <= `LEC_SIMD_SRC1_ADDR_DEFAULT_VALUE;
	else if (signal_lec_simd_src1_addr_we==1'b 1)
		reg_lec_simd_src1_addr <= signal_lec_simd_src1_addr_output;
end
assign signal_lec_simd_src1_addr_input = reg_lec_simd_src1_addr;

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
		reg_lec_simd_dst_addr <= `LEC_SIMD_DST_ADDR_DEFAULT_VALUE;
	else if (signal_lec_simd_dst_addr_we==1'b 1)
		reg_lec_simd_dst_addr <= signal_lec_simd_dst_addr_output;
end
assign signal_lec_simd_dst_addr_input = reg_lec_simd_dst_addr;

always@(posedge clk, negedge rstnn)
begin
	if(rstnn==0)
		reg_lec_simd_num_data <= `LEC_SIMD_NUM_DATA_DEFAULT_VALUE;
	else if (signal_lec_simd_num_data_we==1'b 1)
		reg_lec_simd_num_data <= signal_lec_simd_num_data_output;
end
assign signal_lec_simd_num_data_input = reg_lec_simd_num_data;

assign lec_simd_src0_addr = reg_lec_simd_src0_addr;

assign lec_simd_src1_addr = reg_lec_simd_src1_addr;

assign lec_simd_dst_addr = reg_lec_simd_dst_addr;

assign lec_simd_num_data = reg_lec_simd_num_data;

assign lec_simd_start_write_cmd = signal_lec_simd_start_we;
assign signal_lec_simd_start_input = 0;

assign lec_simd_state_re = signal_lec_simd_state_re;
assign signal_lec_simd_state_input = lec_simd_state_input;

endmodule

/*****************/
/* Custom Region */
/*****************/

// wire clk_system;
// wire clk_core;
// wire clk_core_000;
// wire tick;
// wire gclk_system;
// wire gclk_core;
// wire gclk_core_000;
// wire [(5)-1:0] rstnn_seqeunce;
// wire [(5)-1:0] rst_seqeunce;
// wire rstnn_user;
// wire rst_user;
// wire i_simd_master_clk;
// wire i_simd_master_rstnn;
// wire [(32)-1:0] i_simd_master_shaddr;
// wire [(3)-1:0] i_simd_master_shburst;
// wire i_simd_master_shmasterlock;
// wire [(4)-1:0] i_simd_master_shprot;
// wire [(3)-1:0] i_simd_master_shsize;
// wire [(2)-1:0] i_simd_master_shtrans;
// wire i_simd_master_shwrite;
// wire [(32)-1:0] i_simd_master_shwdata;
// wire i_simd_master_shready;
// wire [(32)-1:0] i_simd_master_shrdata;
// wire i_simd_master_shresp;
// wire i_simd_slave_clk;
// wire i_simd_slave_rstnn;
// wire i_simd_slave_rpsel;
// wire i_simd_slave_rpenable;
// wire i_simd_slave_rpwrite;
// wire [(32)-1:0] i_simd_slave_rpaddr;
// wire [(32)-1:0] i_simd_slave_rpwdata;
// wire i_simd_slave_rpready;
// wire [(32)-1:0] i_simd_slave_rprdata;
// wire i_simd_slave_rpslverr;

/* DO NOT MODIFY THE ABOVE */
/* MUST MODIFY THE BELOW   */

LEC_SIMD
i_simd
(
	.clk(i_simd_master_clk),
	.rstnn(i_simd_master_rstnn),

	.shaddr(i_simd_master_shaddr),
	.shburst(i_simd_master_shburst),
	.shmasterlock(i_simd_master_shmasterlock),
	.shprot(i_simd_master_shprot),
	.shsize(i_simd_master_shsize),
	.shtrans(i_simd_master_shtrans),
	.shwrite(i_simd_master_shwrite),
	.shwdata(i_simd_master_shwdata),
	.shready(i_simd_master_shready),
	.shrdata(i_simd_master_shrdata),
	.shresp(i_simd_master_shresp),

	.rpsel(i_simd_slave_rpsel),
	.rpenable(i_simd_slave_rpenable),
	.rpwrite(i_simd_slave_rpwrite),
	.rpaddr(i_simd_slave_rpaddr),
	.rpwdata(i_simd_slave_rpwdata),
	.rpready(i_simd_slave_rpready),
	.rprdata(i_simd_slave_rprdata),
	.rpslverr(i_simd_slave_rpslverr)
);

assign i_simd_master_clk = clk_core;

#ifndef __H_LEC_SIMD_API_H__
#define __H_LEC_SIMD_API_H__

void lec_simd_set_src0_addr(unsigned int addr);
void lec_simd_set_src1_addr(unsigned int addr);
void lec_simd_set_dst_addr(unsigned int addr);
void lec_simd_set_num_data(unsigned int num_data); // num of byte
void lec_simd_start();
int lec_simd_get_state();
void lec_simd_wait_until_done();

#endif


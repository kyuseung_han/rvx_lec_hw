#include "lec_simd_memorymap.h"
#include "lec_simd_api.h"
#include "ervp_platform_controller_api.h"

static inline int wrong_num = 0;

void lec_simd_set_src0_addr(unsigned int addr)
{
	REG32(MMAP_LEC_SIMD_SRC0_ADDR) = addr;
}

void lec_simd_set_src1_addr(unsigned int addr)
{
	REG32(MMAP_LEC_SIMD_SRC1_ADDR) = addr;
}

void lec_simd_set_dst_addr(unsigned int addr)
{
	REG32(MMAP_LEC_SIMD_DST_ADDR) = addr;
}

void lec_simd_set_num_data(unsigned int num_data)
{
	unsigned int num_data_in_4byte;
	//num_data_in_4byte = ((num_data-1)>>2) + 1;
	num_data_in_4byte = num_data>>2;
	if((num_data_in_4byte&0xf)==0)
		wrong_num = 0;
	else
		wrong_num = 1;
	REG32(MMAP_LEC_SIMD_NUM_DATA) = num_data_in_4byte;
}

void lec_simd_start()
{
	REG32(MMAP_LEC_SIMD_START) = 0;
}

int lec_simd_get_state()
{
	return REG32(MMAP_LEC_SIMD_STATE);
}

void lec_simd_wait_until_done()
{
	if(wrong_num==0)
		while(lec_simd_get_state()!=0);
	else
		while(1);
}

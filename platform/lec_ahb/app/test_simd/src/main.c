#include "platform_info.h"
#include "ervp_printf.h"

#define NUN_DATA 128

volatile unsigned char a[NUN_DATA];
volatile unsigned char b[NUN_DATA];
volatile unsigned char c[NUN_DATA];

int main() {
	int i;
	for(i=0; i<NUN_DATA; i++)
	{
		a[i] = i<<1;
		b[i] = NUN_DATA -i;
		c[i] = 0;
	}
	lec_simd_set_src0_addr(a);
	lec_simd_set_src1_addr(b);
	lec_simd_set_dst_addr(c);
	lec_simd_set_num_data(NUN_DATA);
	lec_simd_start();
	lec_simd_wait_until_done();
	for(i=0; i<NUN_DATA; i++)
	{
		if((i&7)==0)
			printf("\n");
		printf(" %d", c[i]);
	}
	return 0;
}

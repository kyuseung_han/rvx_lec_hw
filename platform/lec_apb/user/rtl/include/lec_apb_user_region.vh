/*****************/
/* Custom Region */
/*****************/

// wire clk_system;
// wire clk_core;
// wire clk_core_000;
// wire tick;
// wire gclk_system;
// wire gclk_core;
// wire gclk_core_000;
// wire [(5)-1:0] rstnn_seqeunce;
// wire [(5)-1:0] rst_seqeunce;
// wire rstnn_user;
// wire rst_user;
// wire i_test1_clk;
// wire i_test1_rstnn;
// wire i_test1_rpsel;
// wire i_test1_rpenable;
// wire i_test1_rpwrite;
// wire [(32)-1:0] i_test1_rpaddr;
// wire [(32)-1:0] i_test1_rpwdata;
// wire i_test1_rpready;
// wire [(32)-1:0] i_test1_rprdata;
// wire i_test1_rpslverr;

/* DO NOT MODIFY THE ABOVE */
/* MUST MODIFY THE BELOW   */

TEST1_APB
#(
	.BW_ADDR((32)),
	.BW_DATA((32))
)
i_test1
(
	.clk(i_test1_clk),
	.rstnn(i_test1_rstnn),
	.rpsel(i_test1_rpsel),
	.rpenable(i_test1_rpenable),
	.rpwrite(i_test1_rpwrite),
	.rpaddr(i_test1_rpaddr),
	.rpwdata(i_test1_rpwdata),
	.rpready(i_test1_rpready),
	.rprdata(i_test1_rprdata),
	.rpslverr(i_test1_rpslverr)
);
